from django.db import models
from datetime import datetime
from users.models import CustomUser as User


class Buildings(models.Model):
    buidling_address = models.CharField(max_length = 100)
    building_zip = models.IntegerField()
    number_of_units = models.IntegerField()
    building_owner = models.CharField(max_length = 100)
    building_slug = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Buidings"
        verbose_name = "Building"

    def __str__(self):
        return self.buidling_address

class Leases(models.Model):
    apartment = models.CharField(max_length=100)
    length = models.IntegerField()
    tenant = models.CharField(max_length=100)
    rent_amount = models.IntegerField()
    day_rent_is_due = models.CharField(max_length=50)
    deposit_amount = models.IntegerField()

    class Meta:
        verbose_name_plural = "Lease"
        verbose_name = "Leases"

    def __str__(self):
        return self.apartment


class Apartments(models.Model):
    apartment_owner = models.CharField(max_length=50)
    building_address = models.ForeignKey(Buildings, verbose_name="Buidlings", on_delete="NONE")
    building_unit_number = models.CharField(max_length=25)
    apartment_slug = models.CharField(max_length=200, default=building_unit_number)
    apartment_bedrooms = models.IntegerField()
    apartment_bathrooms = models.IntegerField()
    apartment_occupied = models.BooleanField()
    apartment_rent = models.IntegerField()
    userRenter = models.ForeignKey(User, verbose_name="Users", on_delete="NONE")
    lease_payment_plan_id = models.CharField(max_length=25, default="NONE")
    class Meta:
        # Gives the proper plural name for admin
        verbose_name_plural = "Apartments"
        verbose_name = "Apartment"
    def __str__(self):
        return self.apartment_slug





