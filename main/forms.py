from django import forms
from django.contrib.auth.forms import UserCreationForm
from users.models import CustomUser
from users.models import CustomUser as User
from .models import Buildings, Leases
 

class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    middle_name = forms.CharField(required=False)
    date_of_birth = forms.DateField(required=True)
    phone = forms.CharField(required=True)
    type_of_user = forms.CharField(initial="tenant",  widget=forms.TextInput(attrs={'readonly':'readonly'}))


    class Meta:
        model = User
        fields = ("username", "email", "first_name", "last_name", "middle_name", "date_of_birth", "phone", "type_of_user", "password1", "password2")

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.middle_name = self.cleaned_data["middle_name"]
        user.date_of_birth = self.cleaned_data["date_of_birth"]
        user.phone = self.cleaned_data["phone"]
        user.type_of_user = self.cleaned_data["type_of_user"]
        if commit:
            user.save()
        return user

class NewLandlordForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    middle_name = forms.CharField(required=False)
    date_of_birth = forms.DateField(required=True)
    phone = forms.CharField(required=True)
    type_of_user = forms.CharField(initial="landlord",  widget=forms.TextInput(attrs={'readonly':'readonly'}))

    class Meta:
        model = User
        fields = ("username", "email", "first_name", "last_name", "middle_name", "date_of_birth", "phone", "type_of_user", "password1", "password2")

    def save(self, commit=True):
        user = super(NewLandlordForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.middle_name = self.cleaned_data["middle_name"]
        user.date_of_birth = self.cleaned_data["date_of_birth"]
        user.phone = self.cleaned_data["phone"]
        user.type_of_user = self.cleaned_data["type_of_user"]

        if commit:
            user.save()
        return user



class AddTenantToLease(forms.ModelForm):
    apartment = forms.CharField(max_length=100)
    length = forms.IntegerField(max_value=24)
    tenant = forms.EmailField(required=True, label="Add tenent by Email Address")
    rent_amount = forms.IntegerField(max_value=5000)
    day_rent_is_due = forms.CharField(max_length=50)
    deposit_amount = forms.IntegerField(max_value=5000)

    class Meta:
        model = Leases
        fields = ("apartment", "length", "tenant", "rent_amount","day_rent_is_due","deposit_amount")




class planForm(forms.Form):
    plan_name = forms.CharField(required=True)
    plan_apt_address = forms.CharField(required = True)
    rent_amount = forms.IntegerField(required=True)
    rent_user_firstname = forms.CharField(max_length=25)
    rent_user_lastname = forms.CharField(max_length=25)
    interval = forms.CharField(max_length=25)
    product = forms.CharField(max_length=25)

    class Meta:
        model = User
        fields = ("plan_name", "plan_apt_address", "rent_amount", "rent_user_firstname","rent_user_lastname","interval","product")



    