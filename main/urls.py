from django.urls import path, include
from . import views
from django.contrib import admin


app_name = 'main'  # here for namespacing of urls.

urlpatterns = [
    path("", views.homepage, name="homepage"),
    path("logout", views.logout_request, name="logout"),
    path("user-register/", views.user_register, name="user-register"),
    path("login/", views.login_request, name="user-login"),
    path("user-overview/", views.user_overview, name="user_overview"),
    path("landlord-register/", views.landlord_register, name="landlord_register"),  
    path("landlord-overview/", views.landlord_overview, name="landlord_overview"),
    path('admin/', admin.site.urls),
    path("<single_slug>/", views.single_slug, name="single_slug"),
]