from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser
from django import forms


class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    middle_name = forms.CharField(required=False)
    date_of_birth = forms.DateField(required=True)
    phone = forms.CharField(required=True)
    type_of_user = forms.CharField(initial="tenant",  widget=forms.TextInput(attrs={'readonly':'readonly'}))
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ("email", "first_name", "last_name", "middle_name", "date_of_birth", "phone", "type_of_user")
    def save(self, commit=True):
        user = super(CustomUserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.middle_name = self.cleaned_data["middle_name"]
        user.date_of_birth = self.cleaned_data["date_of_birth"]
        user.phone = self.cleaned_data["phone"]
        user.type_of_user = self.cleaned_data["type_of_user"]
        if commit:
            user.save()
        return user

class CustomUserChangeForm(UserChangeForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    middle_name = forms.CharField(required=False)
    date_of_birth = forms.DateField(required=True)
    phone = forms.CharField(required=True)
    type_of_user = forms.CharField(initial="tenant",  widget=forms.TextInput(attrs={'readonly':'readonly'}))
    class Meta:
        model = CustomUser
        fields = ("email", "first_name", "last_name", "middle_name", "date_of_birth", "phone", "type_of_user")
    def save(self, commit=True):
        user = super(CustomUserChangeForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.middle_name = self.cleaned_data["middle_name"]
        user.date_of_birth = self.cleaned_data["date_of_birth"]
        user.phone = self.cleaned_data["phone"]
        user.type_of_user = self.cleaned_data["type_of_user"]
        if commit:
            user.save()
        return user

class NewLandlordForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    middle_name = forms.CharField(required=False)
    date_of_birth = forms.DateField(required=True)
    phone = forms.CharField(required=True)
    type_of_user = forms.CharField(initial="landlord",  widget=forms.TextInput(attrs={'readonly':'readonly'}))

    class Meta:
        model = CustomUser
        fields = ("email", "first_name", "last_name", "middle_name", "date_of_birth", "phone", "type_of_user")

    def save(self, commit=True):
        user = super(NewLandlordForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.middle_name = self.cleaned_data["middle_name"]
        user.date_of_birth = self.cleaned_data["date_of_birth"]
        user.phone = self.cleaned_data["phone"]
        user.type_of_user = self.cleaned_data["type_of_user"]

        if commit:
            user.save()
        return user