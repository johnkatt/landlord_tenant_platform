from django.contrib import admin
from .models import Apartments, Buildings
from tinymce.widgets import TinyMCE
from django.db import models




# Register your models here.


admin.site.register(Buildings)
admin.site.register(Apartments)
