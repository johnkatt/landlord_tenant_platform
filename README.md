Tenant and Landlord interaction platform.

The tech used for this build is simply Django (python3) and some materialize css frameworks.

The structure of the project is built into three apps, the build_man which is the root (controlloing site wide settings and admin routes), main (controlling the main views and models for both landlords and tenants) and users (custom user setup apart from the native django model to gain further options).

Tenants and Landlords have a similar register experience with most of the same fields, main difference is the user type flag which will help the app route the end user to the correct pages and functionalities.

Same with the login, both tenants and landlords have the same experience, after logging in the user will be routed to the correct page based on what type of user they are (using the flag). Admin user who sets up the site has admin privileges to the site, and can conduct the initial addition of a landlords buildings and apartments.

Once in, a landlord view will show his buildings currently in the system as well as his apartments. By going into a building details page the landlord is then able to see just the apartments associated with that building. The page will give an overview of the building. And on the cards showing the each apartment the landlord can tell if the unit is rented or not and if it is who, and what the going rent is.
When the landlord goes to the apartment detail view, they can see many of the same details while also being able to see more details on current renter if there is one, or add a new one if there is not a current tenant.

For the tenant view, the user will log in and route to their page. It will show a card only if they are currently marked as a current tenant or if a landlord adds them to a vacant unit. 