from django.shortcuts import render, redirect
from .models import Buildings, Apartments, Leases
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import logout, authenticate, login
from django.contrib import messages
from users.forms import NewLandlordForm, CustomUserCreationForm
from .forms import AddTenantToLease
from django.http import HttpResponse
from django.urls import path, reverse_lazy
from django.views.generic.base import RedirectView
from users.models import CustomUser as User
from django.db.models import F
# Create your views here.



def single_slug(request, single_slug):
    if single_slug == "admin":
        RedirectView.as_view(url=reverse_lazy('admin:index'))

    elif request.user.is_superuser:
        apartments = [c.apartment_slug for c in Apartments.objects.all()]
        if single_slug in apartments:
            apartment = Apartments.objects.filter(apartment_slug__exact = single_slug)
            return render(request = request,
                          template_name='apartment.html',
                          context = {"Apartments": apartment})

        buildings = [t.building_slug for t in Buildings.objects.all()]
        if single_slug in buildings:
            building = Buildings.objects.filter(building_slug__exact = single_slug)
            return render(request = request,
                          template_name='building.html',
                          context = {"Buildings": building})
    else:
        apartments = [c.apartment_slug for c in Apartments.objects.all()]
        if single_slug in apartments:
            apartment = Apartments.objects.filter(apartment_slug__exact = single_slug)
            occupied = Apartments.objects.filter(apartment_slug__exact = single_slug).values_list('apartment_occupied')
            if occupied:
                uid = Apartments.objects.filter(apartment_slug__exact = single_slug).values_list('userRenter_id').first()
                uid = uid[0]
                userpro = User.objects.filter(id=uid)
                form = AddTenantToLease(request.POST)

            else:
                userpro = []

            if request.method == "POST":
                form = AddTenantToLease(request.POST)
                if form.is_valid():
                    apartment = form.cleaned_data['apartment']
                    length = form.cleaned_data['length']
                    tenant = form.cleaned_data['tenant']
                    rent_amount = form.cleaned_data['rent_amount']
                    day_rent_is_due = form.cleaned_data['day_rent_is_due']
                    deposit_amount = form.cleaned_data['deposit_amount']
                    user_check = User.objects.filter(email=tenant).values_list('id').first()
                    if user_check:

                        p = Leases(apartment=apartment, length=length, tenant=tenant, rent_amount=rent_amount, day_rent_is_due=day_rent_is_due, deposit_amount=deposit_amount)
                        p.save()
                        updateApt = Apartments.objects.filter(apartment_slug__exact = single_slug).update(apartment_occupied=(True),apartment_rent=(rent_amount), userRenter_id =(user_check[0]))

                        return redirect("main:landlord_overview")
                    else:
                        for msg in form.error_messages:
                            messages.error(request, f"{msg}: {form.error_messages[msg]}")

                        return render(request = request,
                                    template_name = "apartment.html",
                                    context={"Apartments": apartment, "userpro": userpro, "form":form})

                else:
                    for msg in form.error_messages:
                        messages.error(request, f"{msg}: {form.error_messages[msg]}")

                    return render(request = request,
                                template_name = "apartment.html",
                                context={"Apartments": apartment, "userpro": userpro, "form":form})

            form = AddTenantToLease
            return render(request = request,
                          template_name='apartment.html',
                          context = {"Apartments": apartment, "userpro": userpro, "form":form})

        buildings = [t.building_slug for t in Buildings.objects.all()]
        if single_slug in buildings:
            building = Buildings.objects.filter(building_slug__exact = single_slug)
            apartments_building = Apartments.objects.filter(building_address__exact = building[0])
        
            return render(request = request,
                          template_name='building.html',
                          context = {"Buildings": building, "Apartments":apartments_building})

def homepage(request):
    return render(request = request,
                  template_name='home.html',
                 )


def user_overview(request):
    if request.user.type_of_user == 'landlord':
        messages.info(request, "You are not a tenant!")
        return redirect("main:landlord_overview")

    elif request.user.is_authenticated:
        userKey = request.user
        apartments = Apartments.objects.filter(userRenter__exact=userKey)
        return render(request = request,
                  template_name='tenant.html',
                  context = {"Apartments": apartments})

    else:
        messages.info(request, "Need to Log In!")
        return redirect("main:login_request")


def landlord_overview(request):
    if request.user.type_of_user == "tenant":
        messages.info(request, "You are not a landlord!")
        return redirect("main:tenant_overview")
    elif request.user.is_authenticated:
        userKey = request.user.email
        apartments = Apartments.objects.filter(apartment_owner__exact=userKey)
        buildings = Buildings.objects.filter(building_owner__exact=userKey)
        return render(request = request,
                  template_name='landlord.html',
                  context = {"Apartments": apartments, "Buildings": buildings})
    else:
        messages.info(request, "Need to Log In!")
        return redirect("main:login_request")


def logout_request(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("main:homepage")



    #User Register View
def user_register(request):
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            login(request, user)
            messages.success(request, f"New account created: {username}")
            return redirect("main:homepage")

        else:
            for msg in form.error_messages:
                messages.error(request, f"{msg}: {form.error_messages[msg]}")

            return render(request = request,
                          template_name = "register.html",
                          context={"form":form})

    form = CustomUserCreationForm
    return render(request = request,
                  template_name = "register.html",
                  context={"form":form})



#User and Landlord Login View and handle
def login_request(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('username')
            print(email)
            password = form.cleaned_data.get('password')
            user = authenticate(username=email, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {email}")
                if request.user.type_of_user == "tenant":
                    return redirect('/user-overview')
                else:
                    return redirect('/landlord-overview')
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request = request,
                    template_name = "login.html",
                    context={"form":form})


#Landlord register handle

def landlord_register(request):
    if request.method == "POST":
        form = NewLandlordForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('email')
            login(request, user)
            messages.success(request, f"New account created: {username}")
            return redirect("main:homepage")

        else:
            for msg in form.error_messages:
                messages.error(request, f"{msg}: {form.error_messages[msg]}")

            return render(request = request,
                          template_name = "register.html",
                          context={"form":form})

    form = NewLandlordForm
    return render(request = request,
                  template_name = "register.html",
                  context={"form":form})



